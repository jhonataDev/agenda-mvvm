package com.agenda.agenda.view;

import android.arch.lifecycle.Observer;
import android.arch.lifecycle.ViewModelProviders;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;
import com.agenda.agenda.R;
import com.agenda.agenda.adapter.ContactAdapter;
import com.agenda.agenda.model.Contact;
import com.agenda.agenda.viewmodel.ContactViewModel;

import java.util.List;


public class AgendaListFragment extends Fragment {

    private AgendaFormFragment agendaFormFragment;
    private ContactAdapter mAdapterContact;
    private RecyclerView mRecyclerView;

    private ContactViewModel contactViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View view = inflater.inflate(R.layout.fragment_agenda_list, container, false);

        mRecyclerView = view.findViewById(R.id.contactListId);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        mRecyclerView.setHasFixedSize(true);

        mAdapterContact = new ContactAdapter();
        mRecyclerView.setAdapter(mAdapterContact);

        return view;

    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        showBackButton();

        contactViewModel = ViewModelProviders.of(getActivity()).get(ContactViewModel.class);
        contactViewModel.getAllContacts().observe(getViewLifecycleOwner(), new Observer<List<Contact>>() {
            @Override
            public void onChanged(@Nullable List<Contact> contacts) {
                mAdapterContact.setContacts(contacts);
            }
        });

        Button btnNewContact = view.findViewById(R.id.btnNewContact);
        btnNewContact.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                newContact();
            }
        });

        new ItemTouchHelper(new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.RIGHT) {
            @Override
            public boolean onMove(@NonNull RecyclerView recyclerView, @NonNull RecyclerView.ViewHolder viewHolder, @NonNull RecyclerView.ViewHolder viewHolder1) {
                return false;
            }

            @Override
            public void onSwiped(@NonNull RecyclerView.ViewHolder viewHolder, int i) {
                contactViewModel.delete(mAdapterContact.getContactsAt(viewHolder.getAdapterPosition()));
                Toast.makeText(getContext(), "Contato Deletado", Toast.LENGTH_SHORT).show();
            }
        }).attachToRecyclerView(mRecyclerView);
    }

    private void newContact() {

        agendaFormFragment = new AgendaFormFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameId, agendaFormFragment).addToBackStack(null);
        transaction.commit();
    }

    private void showBackButton() {
        if (getActivity() instanceof AgendaMainActivity) {
            ((AgendaMainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(false);
        }
    }
}