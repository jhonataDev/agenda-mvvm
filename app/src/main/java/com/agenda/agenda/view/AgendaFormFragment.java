package com.agenda.agenda.view;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.Toast;

import com.agenda.agenda.R;
import com.agenda.agenda.model.Contact;
import com.agenda.agenda.viewmodel.ContactViewModel;

public class AgendaFormFragment extends Fragment {

    public static final String EXTRA_NAME = "com.agenda.agenda.view.EXTRA_NAME";
    public static final String EXTRA_PHONE = "com.agenda.agenda.view.EXTRA_PHONE";

    private EditText name;
    private EditText phone;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_agenda_form, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        name = view.findViewById(R.id.formName);
        phone = view.findViewById(R.id.formPhone);
        setHasOptionsMenu(true);
        showBackButton();
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.menu_add_contact, menu);
        super.onCreateOptionsMenu(menu, inflater);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        switch (item.getItemId()) {
            case R.id.save_contact:
                saveContact();
                return true;
            case android.R.id.home:
                getActivity().onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    private void returnFragment() {
        AgendaListFragment agendaListFragment = new AgendaListFragment();
        FragmentTransaction transaction = getFragmentManager().beginTransaction();
        transaction.replace(R.id.frameId, agendaListFragment);
        transaction.commit();
    }

    private void saveContact() {
        String nameContact = name.getText().toString();
        String phoneConatct = phone.getText().toString();

        if (nameContact.trim().isEmpty() || phoneConatct.trim().isEmpty()) {
            Toast.makeText(getContext(), "Por favor, Insira os dados", Toast.LENGTH_SHORT).show();
            return;
        }

        Intent data = new Intent();
        data.putExtra(EXTRA_NAME, nameContact);
        data.putExtra(EXTRA_PHONE, phoneConatct);

        Contact contact = new Contact(nameContact, phoneConatct);
        ContactViewModel nome = new ContactViewModel(getActivity().getApplication());
        nome.insert(contact);

        Toast.makeText(getContext(), "Dados salvos com sucesso!!!", Toast.LENGTH_SHORT).show();
        returnFragment();
    }

    private void showBackButton() {
        if (getActivity() instanceof AgendaMainActivity) {
            ((AgendaMainActivity)getActivity()).getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }
    }
}