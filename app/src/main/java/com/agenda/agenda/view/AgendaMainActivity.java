package com.agenda.agenda.view;

import android.content.Context;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;


import com.agenda.agenda.R;


public class AgendaMainActivity extends AppCompatActivity {

    private Context context;
    private AgendaListFragment agendaListFragment;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_agenda_main);

        initializeVariable();
        initializeAction();
    }

    private void initializeVariable() {
        context = AgendaMainActivity.this;
        agendaListFragment = new AgendaListFragment();
    }

    private void initializeAction() {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.frameId, agendaListFragment);
        transaction.commit();
    }
}