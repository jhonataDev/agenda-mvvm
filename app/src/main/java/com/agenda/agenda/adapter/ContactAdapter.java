package com.agenda.agenda.adapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.agenda.agenda.R;
import com.agenda.agenda.model.Contact;

import java.util.ArrayList;
import java.util.List;

public class ContactAdapter extends RecyclerView.Adapter<ContactAdapter.ViewHolder> {

    private List<Contact> list = new ArrayList<>();

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {
        View view =  LayoutInflater.from(viewGroup.getContext())
                .inflate(R.layout.layout_agenda_list, viewGroup, false);

        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder viewHolder, int position) {
        Contact currentContact = list.get(position);

        viewHolder.name.setText(currentContact.getName());
        viewHolder.phone.setText(currentContact.getPhone());
    }

    @Override
    public int getItemCount() {
            return list.size();
    }

    public void setContacts(List<Contact> contacts){
        this.list = contacts;
        notifyDataSetChanged();
    }

    public Contact getContactsAt(int position){
        return list.get(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder{

        private TextView name;
        private TextView phone;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);

            name = itemView.findViewById(R.id.idContactName);
            phone = itemView.findViewById(R.id.idContactPhone);
        }
    }
}
