package com.agenda.agenda.datasource;

import android.app.Application;
import android.arch.lifecycle.LiveData;
import android.os.AsyncTask;

import com.agenda.agenda.model.Contact;

import java.util.List;

public class ContactRepository {
    private ContactDao contactDao;
    private LiveData<List<Contact>> allContacts;

    public ContactRepository(Application application){
        ContactDatabase database = ContactDatabase.getInstance(application);
        contactDao = database.contactDao();
        allContacts = contactDao.getAllContacts();
    }

    public void insert (Contact contact){
        new InsertContactsAsyncTask(contactDao).execute(contact);
    }

    public void update (Contact contact){
        new UpdateContactsAsyncTask(contactDao).execute(contact);
    }

    public void delete (Contact contact){
        new DeleteContactsAsyncTask(contactDao).execute(contact);
    }

    public LiveData<List<Contact>> getAllContacts() {
        return allContacts;
    }

    private static class InsertContactsAsyncTask extends AsyncTask<Contact, Void, Void>{
        private ContactDao contactDao;

        private InsertContactsAsyncTask(ContactDao contactDao){
            this.contactDao = contactDao;
        }
        @Override
        protected Void doInBackground(Contact... contacts) {
            contactDao.insert(contacts[0]);
            return null;
        }
    }

    private static class UpdateContactsAsyncTask extends AsyncTask<Contact, Void, Void>{
        private ContactDao contactDao;
        private UpdateContactsAsyncTask(ContactDao contactDao){
            this.contactDao = contactDao;
        }

        @Override
        protected Void doInBackground(Contact... contacts) {
            contactDao.update(contacts[0]);
            return null;
        }
    }

    private static class DeleteContactsAsyncTask extends AsyncTask<Contact, Void, Void>{

        private ContactDao contactDao;
        private DeleteContactsAsyncTask(ContactDao contactDao){
            this.contactDao = contactDao;
        }

        @Override
        protected Void doInBackground(Contact... contacts) {
            contactDao.delete(contacts[0]);
            return null;
        }
    }
}
