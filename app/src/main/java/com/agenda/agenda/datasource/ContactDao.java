package com.agenda.agenda.datasource;
import android.arch.lifecycle.LiveData;
import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;
import com.agenda.agenda.model.Contact;
import java.util.List;

@Dao
public interface ContactDao {

    @Insert
    void insert(Contact contacts);

    @Update
    void update(Contact contacts);

    @Delete
    void delete(Contact contacts);

    @Query("SELECT * FROM contacts ORDER BY name")
    LiveData<List<Contact>> getAllContacts();
}
